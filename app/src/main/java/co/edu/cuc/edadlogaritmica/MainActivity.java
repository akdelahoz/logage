package co.edu.cuc.edadlogaritmica;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;


import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.txtMessage) TextView txtMessage;
    @BindView(R.id.cmdCalculate) Button cmdCalculate;
    @BindView(R.id.cmdReset) Button cmdReset;
    @BindView(R.id.dateAge) DatePicker dpiBirthDate;
    @BindView(R.id.txtSalida) TextView txtSalida;
    private final int GESTATION_TIME = 9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //Custom Font
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/spac3.ttf");
        txtMessage.setTypeface(custom_font);
        cmdCalculate.setTypeface(custom_font);
        cmdReset.setTypeface(custom_font);
    }

    public static final int getMonthsDifference(Calendar date1, Calendar date2) {
        int m1 = date1.get(Calendar.YEAR) * 12 + date1.get(Calendar.MONTH);
        int m2 = date2.get(Calendar.YEAR) * 12 + date2.get(Calendar.MONTH);
        return m2 - m1;
    }

    public void onClickCalculate(View view) {
        //Birth Date
        Calendar currentDate = Calendar.getInstance();
        Calendar birthDate = Calendar.getInstance();
        int day = dpiBirthDate.getDayOfMonth();
        int month = dpiBirthDate.getMonth();
        int year = dpiBirthDate.getYear();
        birthDate.set(year, month, day);
        //Difference in Months
        int ageInMonths = getMonthsDifference(birthDate, currentDate);
        //Logarithmic Age
        double logAge = Math.log10(ageInMonths / GESTATION_TIME);

        //Output
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        txtSalida.setText("Su edad es: "+df.format(logAge));
        //Disable controls
        cmdReset.setEnabled(true);
        cmdCalculate.setEnabled(false);
        dpiBirthDate.setEnabled(false);
    }

    public void onClickReset(View view) {
        //Restore Controls
        cmdCalculate.setEnabled(true);
        cmdReset.setEnabled(false);
        dpiBirthDate.setEnabled(true);
        txtSalida.setText(null);
    }

}
